module.exports.run = async (bot, message, args, Discord, db) => {

    embed = new Discord.RichEmbed()
        .setColor('#2D98AA')
        .setAuthor(`Request by ${message.author.username}: `, message.author.avatarURL)
        .setDescription("**=> 2VS2 RANKED <=**\n``duo.create`` (You can add a name for your team if you want). \n``duo.join <id>`` (Join your friend team).\n``duo.leave`` (It do leave your team, if you are the last in the team, it delete).\n``duo.stats <id>`` (Show your team stats, **id** optional).\n\n**=> CLASSIC COMMAND <=**\n``duo.leader`` (Show the best duo !)")
        .setFooter('If you encounter any issues, contact the staff.')
    message.channel.send(embed);
}

module.exports.help = {
    name: ".help"
}