module.exports.run = async (bot, message, args, Discord, db) => {

    post = db.get('team').sortBy('elo').take(4).value();
    
    embed = new Discord.RichEmbed()
        .setColor("#4a69bd")
        .setTitle(':bar_chart: 2vs2 Ranked Leaderboard :')
        .addField(`Top 10 teams :`, /*:::::::::::::::`**Elo** : ${post[9].elo} - **Team** : ${post[9].name}\n**Elo** : ${post[8].elo} - **Team** : ${post[8].name}\n**Elo** : ${post[7].elo} - **Team** : ${post[7].name}\n**Elo** : ${post[6].elo} - **Team** : ${post[6].name}\n**Elo** : ${post[5].elo} - **Team** : ${post[5].name}\n**Elo** : ${post[4].elo} - **Team** : ${post[4].name}\n*/`**Elo** : ${post[3].elo} - **Team** : ${post[3].name}\n**Elo** : ${post[2].elo} - **Team** : ${post[2].name}\n**Elo** : ${post[1].elo} - **Team** : ${post[1].name}\n**Elo** : ${post[0].elo} - **Team** : ${post[0].name}`)

    message.channel.send(embed)

}

module.exports.help = {
    name: "duo.leader"
}