module.exports.run = async (bot, message, args, Discord, db) => {

    // Emojis:
    const success = '<:oui:659767277838794782>';
    const fail = '<:non:659767254040444928>';

    let team;

    // si une team est spécifiée
    if (args[0]) {
        team = db.get('team').find({ id: args[0] }).value();
    } else {
        // sinon on cherche la team dans laquelle il est
        team = db.get('team').find({ member1_id: message.author.id }).value();
        team = (typeof team !== "undefined") ? team : db.get('team').find({ member2_id: message.author.id }).value();
    }

    if (typeof team === "undefined") {
        embed = new Discord.RichEmbed()
            .setColor("#f27252")
            .setTitle(`${fail} Error:`)
            .setDescription(
                args[0] ? `Not found team with ID **${args[0]}**` : `${message.author}, you are not in a team.`
            )

        return message.channel.send(embed)
    }

    embed = new Discord.RichEmbed()
            .setColor("#4a69bd")
            .setTitle(`:bar_chart: ${team.name} [ID: ${team.id}] `)
            .setThumbnail(bot.users.get(team.member1_id).displayAvatarURL)
            .addField("Owner", bot.users.get(team.member1_id))
            .addField("Player #1 :", bot.users.get(team.member1_id), true)
            .addField("Player #2 :", team.member2_id > -1 ? bot.users.get(team.member2_id) : "Empty", true)
            .addBlankField(true)
            .addField("Elo :", team.elo, true)
            .addField("Wins :", team.wins, true)
            .addField("Looses :", team.loose, true)

    return message.channel.send(embed);
}

module.exports.help = {
    name: "duo.stats"
}