module.exports.run = async (bot, message, args, Discord, db) => {

    const success = '<:oui:659767277838794782>';
    const fail = '<:non:659767254040444928>';

    /* récupération d'infos */
    team = db.get('team').find({ member1_id: message.author.id }).value();
    team = (typeof team !== "undefined") ? team : db.get('team').find({ member2_id: message.author.id }).value();

    /* le membre est dans une team? */
    if (typeof team !== "undefined") {
        embed = new Discord.RichEmbed()
            .setColor("#f6b93b")
            .setTitle(`:warning: Warning`)
            .setDescription(`${message.author}, you are already in a team.\nPlease leave it before joining a new team. (\`duo.leave\`)`)

        return message.channel.send(embed)
    }

    /* arguments? */
    if (args.length < 1) {
        embed = new Discord.RichEmbed()
            .setColor("#eb2f06")
            .setTitle(`:x: Error`)
            .setDescription(`Command syntax error. Usage: \`duo.join ID\``)

        return message.channel.send(embed)
    }

    teamQ = db.get('team').find({ id: args[0]})
    team = teamQ.value();

    // la team existe-t-elle?
    if (typeof team === "undefined") {
        embed = new Discord.RichEmbed()
            .setColor("#f6b93b")
            .setTitle(`:warning: Warning`)
            .setDescription(`Not found team with ID **${args[0]}**`)

        return message.channel.send(embed)
    }

    // team full?
    if (team.member2_id > -1) {
        embed = new Discord.RichEmbed()
            .setColor("#f6b93b")
            .setTitle(`:warning: Warning`)
            .setDescription(`This team is already full.`)

        return message.channel.send(embed)
    }

    teamQ.assign({ member2_id: message.author.id }).write();

    embed = new Discord.RichEmbed()
        .setColor("#78e08f")
        .setTitle(`:white_check_mark: Success`)
        .setDescription(`${message.author} just joined **${team.name}** (ID: **${team.id}**) !`)

    message.channel.send(embed)
    return
}

module.exports.help = {
    name: "duo.join"
}