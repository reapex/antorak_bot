module.exports.run = async (bot, message, args, Discord, db) => {

    // Emojis:
    const success = '<:oui:659767277838794782>';
    const fail = '<:non:659767254040444928>';

    // est-ce que le membre a déjà une team?
    hasTeam = db.get('team').find({ member1_id: message.author.id }).value();
    hasTeam = (typeof hasTeam !== "undefined") ? hasTeam : db.get('team').find({ member2_id: message.author.id }).value();

    if ( typeof hasTeam !== "undefined") {
        embed = new Discord.RichEmbed()
            .setColor("#f27252")
            .setTitle(`${fail} Error:`)
            .setDescription(`${message.author}, you already in/have a team: **${hasTeam.id}**`)

        return message.channel.send(embed)
    }

    let name, id

    // génération de l'ID
    id = Math.random().toString(36).substring(2, 7)
    while (db.get('team').find({ id: id }).size() > 0) {
        id = Math.random().toString(36).substring(2, 7)
    }

    // le membre a-t-il indiqué un nom de team?
    if (args.length > 0) {
        if (db.get('team').find({ name: args.join(' ') }).size() == 0) {
            name = args.join(' ')
        } else {
            embed = new Discord.RichEmbed()
            .setColor("#f27252")
            .setTitle(`${fail} Error:`)
            .setDescription(`**${args[0]}** already exists, choose a different name`)

            return message.channel.send(embed)
        }
    } else {
        name = `Team #${id}`
    }

    newTeam = {
        id: id,
        name: name,
        member1_id: message.author.id,
        member2_id: -1,
        elo: 0,
        wins: 0,
        loose: 0
    }
    
    db.get('team')
        .push(newTeam)
        .write()

    embed = new Discord.RichEmbed()
        .setColor("#7bd039")
        .setTitle(`${success} Success:`)
        .setDescription(`${message.author} just created a team **${name}** (ID: **${id}**) !`)

    message.channel.send(embed)
}

module.exports.help = {
    name: "duo.create"
}