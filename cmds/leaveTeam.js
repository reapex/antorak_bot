module.exports.run = async (bot, message, args, Discord, db) => {

    const success = '<:oui:659767277838794782>';
    const fail = '<:non:659767254040444928>';

    /* récupération d'infos */
    team = db.get('team').find({ member1_id: message.author.id }).value();
    isOwner = (typeof team === "undefined") ? false : true;
    team = (typeof team !== "undefined") ? team : db.get('team').find({ member2_id: message.author.id }).value();

    /* le membre est dans une team? */
    if (typeof team === "undefined") {
        embed = new Discord.RichEmbed()
            .setColor("#f6b93b")
            .setTitle(`:warning: Warning`)
            .setDescription(`${message.author}, you are not in a team.`)

        return message.channel.send(embed)
    }

    // proprio ou non?
    if (isOwner) {
        db.get('team').remove({
            id: team.id
        }).write();
    } else {
        db.get('team').find({
            id: team.id
        }).assign({ member2_id: -1 }).write();
    }

    embed = new Discord.RichEmbed()
        .setColor("#78e08f")
        .setTitle(`:white_check_mark: Success`)
        .setDescription(
            isOwner ? `Your team has been deleted successfully.` : `You leaved the team.`
        )

    return message.channel.send(embed)
}

module.exports.help = {
    name: "duo.leave"
}