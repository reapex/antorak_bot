module.exports.run = async (bot, message, args, Discord, db, queue) => {
    message.delete()

    /* emotes */
    const success = '<:oui:659767277838794782>';
    const fail = '<:non:659767254040444928>';

    /* récupération d'infos */
    team = db.get('team').find({ member1_id: message.author.id }).value();
    isOwner = (typeof team === "undefined") ? false : true;
    team = (typeof team !== "undefined") ? team : db.get('team').find({ member2_id: message.author.id }).value();

    /* le membre est dans une team? */
    if (typeof team === "undefined") {
        embed = new Discord.RichEmbed()
            .setColor("#f27252")
            .setTitle(`${fail} Error:`)
            .setDescription(`${message.author}, you need to be in a team!`)

        return message.channel.send(embed)
    }

    /* est-il le proprio de la team? */
    if (!isOwner) {
        embed = new Discord.RichEmbed()
            .setColor("#f27252")
            .setTitle(`${fail} Error:`)
            .setDescription(`${message.author}, you need to be the owner of the team!`)

        return message.channel.send(embed)
    }

    /* la team est-elle déjà dans la liste d'attente? */
    if (queue.get('teamInQueue').find({ id: team.id}).size() == 1) {
        embed = new Discord.RichEmbed()
            .setColor("#f27252")
            .setTitle(`${fail} Error:`)
            .setDescription(`${message.author}, your team is already in the queue!`)

        return message.channel.send(embed)
    }

    /* si il n'y a aucune team en attente, on ajoute la team */
    if (queue.get('teamInQueue').size() < 1) {
        queue.get('teamInQueue')
            .push({
                id: team.id
            })
            .write()
        
        embed = new Discord.RichEmbed()
            .setColor("#f27252")
            .setTitle(`${fail} Success:`)
            .setDescription(`${message.author}, your team has been added successfully to the queue!`)

        return message.channel.send(embed)
    }

    /* sinon on "rejoint" les teams */
    nextTeam = db.get('team').find({ id: queue.get('teamInQueue').first().value().id }).value();

    embed = new Discord.RichEmbed()
        .setColor("#f27252")
        .setTitle(`New 2vs2 buildfight ranked match find !`)
        .setDescription(`**Team 1 owner** : <@${team.member1_id}>\n**Team 2 owner** : <@${nextTeam.member1_id}>\n\n**RULES :**\nThe first team to reach 3 wins wins the match, in case of dispute please notify a member of staff !`)

    /* supression de la team de la liste d'attente */
    queue.get('teamInQueue').remove({
        id: nextTeam.id
    }).write();
    
    /* création du channel */
    await message.guild.createChannel(`boxfight_${team.id}`, {
        type: 'text',
        permissionOverwrites: [
            {
                id: message.guild.id,
                deny: ['VIEW_CHANNEL'],
            },
            {
                id: message.author.id,
                allow: ['VIEW_CHANNEL'],
            },
            {
                id: nextTeam.member1_id,
                allow: ['VIEW_CHANNEL']
            }
        ],
    }).then(e => e.send(embed)).then(async function (message) {
        await message.react(bot.emojis.get("667426874212745279"))
        await message.react(bot.emojis.get("667427651287252993"))
        await message.react('❌')

        bot.on('messageReactionAdd', (channel, reaction, user) => {
            if (reaction.emoji.name === "❌") {
                if (user.id === '648928352018628618') return
                channel.delete()
            }
        })

    });

}

module.exports.help = {
    name: "duo.find"
}