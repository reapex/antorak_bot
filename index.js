const config = require('./config.json');
const token = config.token;
const prefix = config.prefix;
const Discord = require('discord.js');
const bot = new Discord.Client();
const fs = require('fs');

const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

const adapter = new FileSync('team.json')
const db = low(adapter)

// lowdb default
db.defaults({ team: [] })
    .write()

const adapterQueue = new FileSync('queue.json')
const queue = low(adapterQueue)

// lowdb default
queue.defaults({ teamInQueue: [] })
    .write()

// Fonctions:

// Login:
bot.login(token);

// Bot connecté:
bot.on('ready', () => {
    console.log('Ready!');
    bot.user.setPresence({
        game: {
            name: 'Anorak PP Custom',
            type: "STREAMING",
            url: "https://www.twitch.tv/antoraktv"
        }
    });
});

// Handler: 
bot.commands = new Discord.Collection();

fs.readdir("./cmds/", (err, files) => {

    if (err) return console.log(err);
    let jsfiles = files.filter(f => f.split(".").pop() === "js");
    if (jsfiles.length <= 0) {
        console.log('Pas de commande à charger !')
        return;
    }

    console.log(`Chargement de ${jsfiles.length} commandes !`);

    jsfiles.forEach((f, i) => {
        let props = require(`./cmds/${f}`);
        console.log(`${i + 1}: ${f} chargées !`);
        bot.commands.set(props.help.name, props);
    });
});

bot.on("message", async message => {
    if (message.channel.id === '665613255225638913' && message.author.id === '664108373146468371') {
        await message.delete('30000')
    }
    if (message.channel.id === '665613255225638913' && message.author.id != '664108373146468371') {
        await message.delete()
    }
    
    if (message.author.bot) return; // Si message du bot
    if (message.channel.type === "dm") return; // Si MP
    //if (message.channel.id !== "665613255225638913")return; //Si envoyé dans un channel random
    let messageArray = message.content.split(/\s+/g);
    let command = messageArray[0];
    //Arguments
    let args = message.content.split(" ").slice(1, message.content.split(" ").length); // on exclut le premier car c'est la commande

    if (!command.startsWith(prefix)) return;
    let cmd = bot.commands.get(command.slice(prefix.length));
    if (cmd) cmd.run(bot, message, args, Discord, db, queue);
})